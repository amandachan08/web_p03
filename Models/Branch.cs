﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json.Serialization;

namespace web_s10196608.Models
{
    public class Branch
    {
        //BranchNo
        [Display(Name = "ID")]
        public int BranchNo { get; set; }

        //Address
        public string Address { get; set; }

        //Telephone
        [RegularExpression(@"[689]\d{7}|\+65[689]\d{7}$",
         ErrorMessage = "Invalid Singapore Phone Number.")]
        public string Telephone { get; set; }

    }
}
