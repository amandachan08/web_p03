﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace web_s10196608.Models
{
    public class Staff
    {
        // Staff ID
        [Display(Name = "ID")]
        public int StaffId { get; set; }

        // Name
        [Required(ErrorMessage = "Name is required.")]
        [StringLength(50, ErrorMessage = "Name cannot exceed 50 characters. ")]
        public string Name { get; set; }

        // Gender
        public char Gender { get; set; }

        // DOB
        [Display(Name = "Date of Birth")]
        [DataType(DataType.Date)]
        public DateTime? DOB { get; set; }

        // Nationality 
        public string Nationality { get; set; }

        // Email
        [Display(Name = "Email Address")]
        [EmailAddress]
        // Custom Validation Attribute for checking email address exists
        [ValidateEmailExists]
        public string Email { get; set; }

        // Salary 
        [Display(Name = "Monthly Salary (SGD)")]
        [DisplayFormat(DataFormatString = "{0:#,##0.00}")]
        [Range(1.00, 10000.00, ErrorMessage = "Salary must be between 1.00 to 10000.00")]
        public decimal Salary { get; set; }

        // Is Full Time
        [Display(Name = "Full-Time Staff")]
        public bool IsFullTime { get; set; }

        // Branch No
        [Display(Name = "Branch")]
        public int? BranchNo { get; set; }


    }
}
