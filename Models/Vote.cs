﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace web_s10196608.Models
{
    public class Vote
    {
        /*  Returned code/data
    "id": 1654,
    "bookId": 1,
    "title": "Eloquent JavaScript, Second Edition",
    "justification": "Please order more of this book. I need it for research!",
    "createdAt": "2020-07-13T13:24:45.9884406+00:00"
         */
        [Display(Name = "ID")]
        public int Id { get; set; }

        [Display(Name = "Book ID")]
        public int BookId { get; set; }

        [Display(Name = "Title")]
        public string Title { get; set; }

        [Display(Name = "Jutisfication")]
        public string Justification { get; set; }

        [Display(Name = "Created At")]
        public DateTime CreatedAt { get; set; }
    }
}
